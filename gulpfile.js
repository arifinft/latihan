var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');


gulp.task('serve', function() {
 browserSync.init(["assets/css/.css", "js/.js"], {
        server: {
            baseDir: "./"
        }
    });
 gulp.watch('assets/scss/*.scss').on('change', function() {
    return gulp.src("assets/scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("assets/css"))
        .pipe(browserSync.stream());
});
  gulp.watch('assets/js/*.js');
  gulp.watch('assets/img/*');
 gulp.watch("*.html").on('change', browserSync.reload);
});